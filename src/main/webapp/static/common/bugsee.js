/**
 * Created by echo on 2017/5/11 0011.
 */
$(document).ready(function() {
    $('.datepicker').datepicker();
    try{
        // 链接去掉虚框
        $("a").bind("focus",function() {
            if(this.blur) {this.blur()};
        });
        //所有下拉框使用select2
        $("select").select2();
    }catch(e){
        // blank
    }
});

// 显示加载框
function loading(mess){
    if (mess == undefined || mess == ""){
        mess = "正在提交，请稍等...";
    }
    resetTip();
    $.jBox.tip(mess,'loading',{opacity:0});
}

// 恢复提示框显示
function resetTip(){
    $.jBox.tip.mess = null;
}

// 确认对话框
function confirmx(mess, href, closed){
    top.$.jBox.confirm(mess,'系统提示',function(v,h,f){
        if(v=='ok'){
            if (typeof href == 'function') {
                href();
            }else{
                resetTip(); //loading();
                location = href;
            }
        }
    },{buttonsFocus:1, closed:function(){
        if (typeof closed == 'function') {
            closed();
        }
    }});
    top.$('.jbox-body .jbox-icon').css('top','55px');
    return false;
}