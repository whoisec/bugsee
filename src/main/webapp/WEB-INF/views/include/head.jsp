<%@ page contentType="text/html;charset=UTF-8" %>

<!-- page level plugin styles -->
<link rel="stylesheet" href="/static/vendor/sortable/css/sortable-theme-bootstrap.css">
<link rel="stylesheet" href="/static/vendor/chosen_v1.4.0/chosen.min.css">
<link rel="stylesheet" href="/static/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<!-- /page level plugin styles -->
<!-- build:css({.tmp,app}) styles/app.min.css -->
<link rel="stylesheet" href="/static/vendor/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/styles/font-awesome.css">
<link rel="stylesheet" href="/static/styles/themify-icons.css">
<link rel="stylesheet" href="/static/styles/animate.css">
<link rel="stylesheet" href="/static/styles/sublime.css">
<!-- endbuild -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- load modernizer -->
<script src="/static/vendor/modernizr.js"></script>
<!--customer js-->
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>

<script src="${ctxStatic}/common/browser.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.js" type="text/javascript"></script>

<link href="${ctxStatic}/jquery-select2/3.4/select2.min.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-select2/3.4/select2.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="/static/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
<script src="/static/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

<script src="${ctxStatic}/common/bugsee.js" type="text/javascript"></script>
<script type="text/javascript">var ctx = '${ctx}', ctxStatic='${ctxStatic}';</script>
