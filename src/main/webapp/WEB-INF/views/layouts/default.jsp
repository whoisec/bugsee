<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html style="overflow-x:auto;overflow-y:auto;">
<head>
	<title><sitemesh:title/>洞察</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>		
	<sitemesh:head/>
</head>
<body>
<div class="app">
	<section class="layout">
		<!-- main content -->
		<section class="main-content">
			<!-- content wrapper -->
			<div class="content-wrap">
				<!-- inner content wrapper -->
				<div class="wrapper">
					<div class="row">
						<div class="col-lg-12">
							<section class="panel">
								<sitemesh:body/>
							</section>
						</div>
					</div>
				</div>
				<!-- /inner content wrapper -->
			</div>
			<!-- /content wrapper -->
			<a class="exit-offscreen"></a>
		</section>
		<!-- /main content -->
	</section>
</div>
</body>
</html>