<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>角色管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        function page(n,s){
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/log/list");
            $("#searchForm").submit();
            return false;
        }
    </script>
</head>
<body>
    <header class="panel-heading no-b">
    <h5><b>字典管理</b></h5>
    </header>
    <div class="panel-body">
    <div class="table-responsive">
        <form:form id="searchForm" action="${ctx}/sys/log/" method="post" class="form-inline" cssStyle="padding-bottom: 10px">
            <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
            <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                <label>地址：</label><input id="title" name="title" type="search" class="form-control" maxlength="50" value="${log.title}"/>
                <label>URL：</label><input id="requestUri" name="requestUri" type="text" class="form-control" maxlength="50" class="input-mini" value="${log.requestUri}"/>
                <label>起始日期：</label>
                    <input id="beginDate" name="beginDate" type="text" readonly="readonly" maxlength="20" class="form-control datepicker" value="<fmt:formatDate value="${log.beginDate}" pattern="yyyy-MM-dd"/> "/>&nbsp;&nbsp;
                <label>结束日期：</label>
                    <input id="endDate" name="endDate" type="text" readonly="readonly" maxlength="20" class="form-control datepicker" value="<fmt:formatDate value="${log.endDate}" pattern="yyyy-MM-dd"/> "/>&nbsp;&nbsp;
            <label for="exception"><input id="exception" name="exception" type="checkbox"${log.exception eq '1'?' checked':''} value="1"/>只查询异常信息</label>
            <button type="submit" class="btn btn-primary">查询</button>
        </form:form>
        <sys:message content="${message}"/>
        <table class="table table-striped table-bordered responsive no-m">
            <thead><tr><th>操作菜单</th><th>操作用户</th><th>URI</th><th>提交方式</th><th>操作者IP</th><th>操作时间</th></thead>
            <tbody><%request.setAttribute("strEnter", "\n");request.setAttribute("strTab", "\t");%>
            <c:forEach items="${page.list}" var="log">
                <tr>
                    <td>${log.title}</td>
                    <td>${log.createBy.loginName}</td>
                    <td><strong>${log.requestUri}</strong></td>
                    <td>${log.method}</td>
                    <td>${log.remoteAddr}</td>
                    <td><fmt:formatDate value="${log.createDate}" type="both"/></td>
                </tr>
                <c:if test="${not empty log.exception}"><tr>
                    <td colspan="8" style="word-wrap:break-word;word-break:break-all;">
                        异常信息: <br/>
                            ${fn:replace(fn:replace(fns:escapeHtml(log.exception), strEnter, '<br/>'), strTab, '&nbsp; &nbsp; ')}</td>
                </tr></c:if>
            </c:forEach>
            </tbody>
        </table>
        <div class="text-center">${page}</div>
    </div>
    </div>
</body>
</html>
