<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>人员编辑</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#loginName").focus();
            $("#inputForm").validate({
                rules: {
                    loginName: {remote: "${ctx}/sys/user/checkLoginName?oldLoginName=" + encodeURIComponent('${user.loginName}')}
                },
                messages: {
                    loginName: {remote: "用户登录名已存在"},
                    confirmNewPassword: {equalTo: "输入与上面相同的密码"}
                },
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <header class="panel-heading">人员编辑</header>
    <div class="panel-body">
        <form:form id="inputForm" modelAttribute="user" action="${ctx}/sys/user/save" method="post" class="form-horizontal">
            <sys:message content="${message}"/>
            <form:hidden path="id"/>
            <div class="form-group">
                <label class="col-sm-2 control-label">用户名：</label>
                <div class="col-sm-3">
                    <input id="oldLoginName" name="oldLoginName" type="hidden" value="${user.loginName}">
                    <form:input path="loginName" htmlEscape="false" maxlength="50" class="required userName form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">密码：</label>
                <div class="col-sm-3">
                    <input id="newPassword" name="newPassword" type="password" value="" maxlength="50" minlength="3" class="${empty user.id?'required':''} form-control"/>
                    <c:if test="${empty user.id}"><font color="red">*</font></c:if>
                    <c:if test="${not empty user.id}"><span class="help-inline">若不修改密码，请留空。</span></c:if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">确认密码：</label>
                <div class="col-sm-3">
                    <input id="confirmNewPassword" name="confirmNewPassword" class="form-control" type="password" value="" maxlength="50" minlength="3" equalTo="#newPassword"/>
                    <c:if test="${empty user.id}"><span class="help-inline"><font color="red">*</font> </span></c:if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">邮箱：</label>
                <div class="col-sm-3">
                    <form:input path="email" htmlEscape="false" maxlength="50" class="required email form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">电话：</label>
                <div class="col-sm-3">
                    <form:input path="phone" htmlEscape="false" maxlength="50" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">备注：</label>
                <div class="col-sm-4">
                    <form:textarea path="remarks" htmlEscape="false" class="form-control" rows="3"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">用户角色：</label>
                <div class="col-sm-5">
                    <form:checkboxes path="roleIdList" items="${allRoles}" itemLabel="name" itemValue="id" htmlEscape="false" style="margin-right:8px;margin-left:10px;" class="required"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">上次登陆</label>
                <div class="col-sm-5">
                    <label class="control-label">IP: ${user.oldLoginIp}&nbsp;&nbsp;&nbsp;&nbsp;时间：<fmt:formatDate value="${user.oldLoginDate}" type="both" dateStyle="full"/></label>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm loading-demo mr5" ><i class="ti-share mr5"></i>Submit</button>
            <input id="btnCancel" class="btn btn-default" type="button" value="返 回" onclick="history.go(-1)"/>
        </form:form>
    </div>
</body>
</html>
