<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!doctype html>
<html class="no-js" lang="">
<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- /meta -->
  <title>${fns:getConfig('productName')}</title>
  <%@ include file="/WEB-INF/views/include/style.jspf" %>
</head>
<!-- body -->
<body>
<div id="container" class="app">
  <!--header start-->
  <header class="header header-fixed navbar" id="header">
    <div class="brand">
      <!-- toggle offscreen menu -->
      <a href="javascript:;" class="ti-menu off-left visible-xs" data-toggle="offscreen" data-move="ltr"></a>
      <!-- /toggle offscreen menu -->
      <!-- logo -->
      <a href="${ctx}" class="navbar-brand">
        <img src="${ctxStatic}/images/logo.png" alt="">
        <span class="heading-font">${fns:getConfig('productName')}</span>
      </a>
      <!-- /logo -->
    </div>
    <ul class="nav navbar-nav">
      <li class="hidden-xs">
        <!-- toggle small menu -->
        <a href="javascript:;" class="toggle-sidebar">
          <i class="ti-menu"></i>
        </a>
        <!-- /toggle small menu -->
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class="off-right">
        <a href="javascript:void(0)" data-toggle="dropdown">
          <img src="${ctxStatic}/images/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">
          <span class="hidden-xs ml10">${fns:getUser().loginName}</span>
          <i class="ti-angle-down ti-caret hidden-xs"></i>
        </a>
        <ul class="dropdown-menu animated fadeInLeft">
          <li><a href="javascript:;">Settings</a></li>
          <li><a href="javascript:;">Upgrade</a></li>
          <li>
            <a href="javascript:;">
              <div class="badge bg-danger pull-right">3</div>
              <span>Notifications</span>
            </a>
          </li>
          <li><a href="${ctx}/logout">Logout</a></li>
        </ul>
      </li>
    </ul>
  </header>
  <!--header end-->
  <section class="layout">
    <!--sidebar start-->
    <%@ include file="/WEB-INF/views/include/menu.jspf" %>
    <!--sidebar end-->
    <!--main content start-->
    <section class="main-content">
      <!-- content wrapper -->
      <div class="content-wrap" id="main-content">

        <iframe id="mainFrame" name="mainFrame" src="" style="overflow:visible;" scrolling="yes" frameborder="no" width="100%" height="100%"></iframe>

      </div>
      <!-- /content wrapper -->
      <a class="exit-offscreen"></a>
    </section>
    <!--main content end-->
  </section>
</div>
<div id="ocean-modal-dialog-div">

</div>

<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="${ctxStatic}/vendor/jquery/dist/jquery.js"></script>
<script src="${ctxStatic}/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="${ctxStatic}/vendor/slimScroll/jquery.slimscroll.js"></script>
<script src="${ctxStatic}/vendor/jquery.easing/jquery.easing.js"></script>
<script src="${ctxStatic}/vendor/jquery_appear/jquery.appear.js"></script>
<script src="${ctxStatic}/vendor/jquery.placeholder.js"></script>
<script src="${ctxStatic}/vendor/fastclick/lib/fastclick.js"></script>
<!-- endbuild -->
<!-- page level scripts -->
<script src="${ctxStatic}/vendor/blockUI/jquery.blockUI.js"></script>
<script src="${ctxStatic}/vendor/jquery.sparkline.js"></script>
<script src="${ctxStatic}/vendor/flot/jquery.flot.js"></script>
<script src="${ctxStatic}/vendor/flot/jquery.flot.resize.js"></script>
<script src="${ctxStatic}/vendor/jquery-countTo/jquery.countTo.js"></script>
<!-- /page level scripts -->
<!-- page script -->
<!-- /page script -->
<!-- template scripts -->
<script src="${ctxStatic}/scripts/offscreen.js"></script>
<script src="${ctxStatic}/scripts/main.js"></script>
<!-- /template scripts -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#ti-settings").click();
    });
</script>
</body>
</html>
