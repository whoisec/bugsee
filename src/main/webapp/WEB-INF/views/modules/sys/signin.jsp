<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!doctype html>
<html class="signin no-js" lang="">

<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- /meta -->

  <title>${fns:getConfig('productName')}</title>

  <!-- page level plugin styles -->
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="/static/vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/styles/font-awesome.css">
  <link rel="stylesheet" href="/static/styles/themify-icons.css">
  <link rel="stylesheet" href="/static/styles/animate.css">
  <link rel="stylesheet" href="/static/styles/sublime.css">
  <!-- endbuild -->



  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <!-- load modernizer -->
  <script src="vendor/modernizr.js"></script>
  <script src="${ctxStatic}/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $("#loginForm").validate({
              rules: {
                  validateCode: {remote: "${pageContext.request.contextPath}/servlet/validateCodeServlet"}
              },
              messages: {
                  username: {required: "请填写用户名."},password: {required: "请填写密码."},
                  validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
              },
              errorLabelContainer: "#messageBox",
              errorPlacement: function(error, element) {
                  error.appendTo($("#loginError").parent());
              }
          });
      });
      // 如果在框架或在对话框中，则弹出提示并跳转到首页
      if(self.frameElement && self.frameElement.tagName == "IFRAME" || $('#left').length > 0 || $('.jbox').length > 0){
          alert('未登录或登录超时。请重新登录，谢谢！');
          top.location = "${ctx}";
      }
  </script>
</head>

<body class="bg-primary">

  <div class="cover" style="background-image: url(/static/images/cover3.jpg)"></div>

  <div class="overlay bg-primary"></div>
  <div class="center-wrapper">
    <div class="center-content">
      <div class="row no-m">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
          <section class="panel bg-white no-b">
            <ul class="switcher-dash-action">
              <li class="active"><a href="#" class="selected">Sign in</a>
              </li>
              <li><a href="${ctx}/signup" class="">New account</a>
              </li>
            </ul>
            <div class="p15">
              <form role="form" action="${ctx}/login" method="post">
                <input type="text" name="username" class="form-control input-lg mb20" placeholder="Username" autofocus>
                <input type="password" name="password" class="form-control input-lg mb20" placeholder="Password">
                <%--<c:if test="${isValidateCodeLogin}"><div class="validateCode">--%>
                  <%--<label class="input-label mid" for="validateCode">验证码</label>--%>
                  <%--<sys:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>--%>
                <%--</div></c:if>--%>
                <div class="show">
                  <label class="checkbox">
                    <input type="checkbox" id="rememberMe" name="rememberMe" ${rememberMe ? 'checked' : ''}/>Remember me &nbsp; <label id="loginError" class="error">${message}</label>
                  </label>
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit">Sign in</button>
              </form>
            </div>
          </section>
          <p class="text-center">
            Copyright &copy;
            <span id="year" class="mr5"></span>
            <span>${fns:getConfig('productName')}</span>
          </p>
        </div>
      </div>

    </div>
  </div>
  <script type="text/javascript">
    var el = document.getElementById("year"),
      year = (new Date().getFullYear());
    el.innerHTML = year;
  </script>
</body>

</html>
