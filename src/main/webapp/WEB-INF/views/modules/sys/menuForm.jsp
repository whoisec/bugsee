<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>

<head>
    <title>菜单编辑</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#name").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <header class="panel-heading">个人信息</header>
    <div class="panel-body">
        <form:form id="inputForm" modelAttribute="menu" action="${ctx}/sys/menu/save" method="post" class="form-horizontal">
            <sys:message content="${message}"/>
            <form:hidden path="id"/>
            <input type="hidden" name="parent.id" value="${menu.parent.id}">
            <div class="form-group">
                <label class="col-sm-2 control-label">上级菜单：</label>
                <div class="col-sm-3">
                    <input type="text" value="${menu.parent.name}" readonly="true" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">名称：</label>
                <div class="col-sm-3">
                    <form:input path="name" htmlEscape="false" maxlength="50" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">链接：</label>
                <div class="col-sm-5">
                    <form:input path="href" htmlEscape="false" maxlength="2000" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">排序：</label>
                <div class="col-sm-2">
                    <form:input path="sort" htmlEscape="false" maxlength="50" class="required digits form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">可见:</label>
                <div class="controls">
                    <form:radiobuttons path="isShow" items="${fns:getDictList('show_hide')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
                    <span class="help-inline">该菜单或操作是否显示到系统菜单中</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">权限标识：</label>
                <div class="col-sm-5">
                    <form:input path="permission" htmlEscape="false" maxlength="100" class="form-control"/>
                    <span class="help-inline">控制器中定义的权限标识，如：@RequiresPermissions("权限标识")</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">备注：</label>
                <div class="col-sm-4">
                    <form:textarea path="remarks" htmlEscape="false" class="form-control" rows="3"/>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm loading-demo mr5" >
                <i class="ti-share mr5"></i>Submit</button>
        </form:form>
    </div>
</body>
</html>
