<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!doctype html>
<html class="signup no-js" lang="">

<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta name="description" content="Flat, Clean, Responsive, application admin template built with bootstrap 3">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- /meta -->

  <title>Sublime - Web Application Admin Dashboard</title>

  <!-- page level plugin styles -->
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="/static/vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/styles/font-awesome.css">
  <link rel="stylesheet" href="/static/styles/themify-icons.css">
  <link rel="stylesheet" href="/static/styles/animate.css">
  <link rel="stylesheet" href="/static/styles/sublime.css">
  <!-- endbuild -->

  <!-- HTML5 shim
   and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <!-- load modernizer -->
  <script src="/static/vendor/modernizr.js"></script>




  <script type="text/javascript" src="/static/jquery-jbox/2.3/jquery.min.js"></script>
  <link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
  <script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>

  <script src="/static/common/browser.js" type="text/javascript"></script>
  <link href="/static/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
  <script src="/static/jquery-jbox/2.3/jquery.jBox-2.3.js" type="text/javascript"></script>
  <script src="/static/common/bugsee.js" type="text/javascript"></script>


  <script type="text/javascript">
      $(document).ready(function() {
          $("#inputForm").validate({
              rules: {
                  loginName: {remote: "${ctx}/sys/user/checkLoginName?oldLoginName=" + encodeURIComponent('${user.loginName}')}
              },
              messages: {
                  loginName: {remote: "用户登录名已存在"},
                  confirmNewPassword: {equalTo: "输入与上面相同的密码"}
              },
              submitHandler: function(form){
                  form.submit();
              },
              errorContainer: "#messageBox",
              errorPlacement: function(error, element) {
                  $("#messageBox").text("输入有误，请先更正。");
                  if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                      error.appendTo(element.parent().parent());
                  } else {
                      error.insertAfter(element);
                  }
              }
          });
      });
  </script>
</head>

<body class="bg-info">
  <div class="center-wrapper">
    <div class="center-content">
      <div class="row no-m">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
          <section class="panel bg-white no-b">
            <ul class="switcher-dash-action">
              <li><a href="${ctx}/signin" class="selected">Sign in</a>
              </li>
              <li class="active"><a href="#" class="">New account</a>
              </li>
            </ul>
            <div class="p15">
              <form:form id="inputForm" modelAttribute="user" action="${ctx}/sys/user/signupSave" method="post">
                <c:if test="${message !=null}">
                  <input type="text" class="form-control input-lg mb25" value="${message}" style="border: 0px" placeholder="ERROR MESSAGE">
                </c:if>
                <input type="text" id="loginName" name="loginName" class="required form-control input-lg mb25" placeholder="Choose a username" autofocus>
                <input type="text" name="email" class="required email form-control input-lg mb25" placeholder="Email address">
                <input type="password" id="newPassword" name="newPassword" class="required form-control input-lg mb25" placeholder="Password">
                <input type="password" id="confirmNewPassword" name="confirmNewPassword" class="required form-control input-lg mb25" placeholder="Confirm password" equalTo="#newPassword">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Sign up</button>
              </form:form>
            </div>
          </section>
          <p class="text-center">
            Copyright &copy;
            <span id="year" class="mr5"></span>
            <span>${fns:getConfig('productName')}</span>
          </p>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var el = document.getElementById("year"),
      year = (new Date().getFullYear());
    el.innerHTML = year;
  </script>
</body>

</html>
