<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>用户管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        function page(n,s){
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/user/list");
            $("#searchForm").submit();
            return false;
        }
    </script>
</head>
<body>
    <header class="panel-heading no-b">
        <h5><b>用户管理</b></h5>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            <form:form id="searchForm" action="${ctx}/sys/user/" method="post" class="form-inline" cssStyle="padding-bottom: 10px">
                <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                <label>用户名：</label><input id="title" name="loginName" type="search" class="form-control" maxlength="50" value="${user.loginName}"/>
                <button type="submit" class="btn btn-primary">搜索</button>
            </form:form>
            <sys:message content="${message}"/>
            <table class="table table-striped table-bordered responsive no-m" data-sortable>
                <thead><tr><th>登录名</th><th>电话</th><th>邮箱</th><shiro:hasPermission name="sys:user:edit"><th>操作</th></shiro:hasPermission></tr></thead>
                <tbody>
                <c:forEach items="${page.list}" var="user">
                    <tr>
                        <td><a href="${ctx}/sys/user/form?id=${user.id}">${user.loginName}</a></td>
                        <td>${user.phone}</td>
                        <td>${user.email}</td>
                        <shiro:hasPermission name="sys:user:edit"><td>
                            <a href="${ctx}/sys/user/form?id=${user.id}">修改</a>
                            <a href="${ctx}/sys/user/delete?id=${user.id}" onclick="return confirmx('确认要删除该用户吗？', this.href)">删除</a>
                        </td></shiro:hasPermission>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="text-center">${page}</div>
        </div>
    </div>
</body>
</html>
