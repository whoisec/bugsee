<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>角色管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        function page(n,s){
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/user/list");
            $("#searchForm").submit();
            return false;
        }
    </script>
</head>
<body>
    <header class="panel-heading no-b">
        <h5><b>角色管理</b></h5>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            <form:form id="searchForm" action="${ctx}/sys/role/" method="post" class="form-inline" cssStyle="padding-bottom: 10px">
                <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                <label>用户名：</label><input id="title" name="name" type="search" class="form-control" maxlength="50" value="${role.name}"/>
                <button type="submit" class="btn btn-primary">搜索</button>
            </form:form>
            <sys:message content="${message}"/>
            <table class="table table-striped table-bordered responsive no-m" data-sortable>
                <thead><tr><th>角色名称</th><th>英文名称</th><th>数据范围</th><shiro:hasPermission name="sys:role:edit"><th>操作</th></shiro:hasPermission></tr></thead>
                <tbody>
                <c:forEach items="${list}" var="role">
                    <tr>
                        <td><a href="form?id=${role.id}">${role.name}</a></td>
                        <td><a href="form?id=${role.id}">${role.enname}</a></td>
                        <td>${fns:getDictLabel(role.dataScope, 'sys_data_scope', '无')}</td>
                        <shiro:hasPermission name="sys:role:edit"><td>
                            <a href="${ctx}/sys/role/form?id=${role.id}">修改</a>
                            <a href="${ctx}/sys/role/delete?id=${role.id}" onclick="return confirmx('确认要删除该角色吗？', this.href)">删除</a>
                        </td></shiro:hasPermission>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
