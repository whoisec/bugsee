<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>菜单编辑</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#value").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <header class="panel-heading">个人信息</header>
    <div class="panel-body">
        <form:form id="inputForm" modelAttribute="dict" action="${ctx}/sys/dict/save" method="post" class="form-horizontal">
            <%--<sys:message content="${message}"/>--%>
            <form:hidden path="id"/>
            <div class="form-group">
                <label class="col-sm-2 control-label">键值：</label>
                <div class="col-sm-3">
                    <form:input path="value" htmlEscape="false" maxlength="50" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">标签：</label>
                <div class="col-sm-3">
                    <form:input path="label" htmlEscape="false" maxlength="50" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">类型：</label>
                <div class="col-sm-3">
                    <form:input path="type" htmlEscape="false" maxlength="50" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">描述：</label>
                <div class="col-sm-3">
                    <form:input path="description" htmlEscape="false" maxlength="50" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">排序：</label>
                <div class="col-sm-3">
                    <form:input path="sort" htmlEscape="false" maxlength="11" class="required digits form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">备注：</label>
                <div class="col-sm-4">
                    <form:textarea path="remarks" htmlEscape="false" class="form-control" rows="3"/>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm loading-demo mr5" >
                <i class="ti-share mr5"></i>Submit</button>
        </form:form>
    </div>
</body>
</html>
