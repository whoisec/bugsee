<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>个人信息</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <header class="panel-heading">个人信息</header>
    <div class="panel-body">
        <sys:message content="${message}"/>
        <form:form id="inputForm" modelAttribute="user" action="${ctx}/sys/user/info" method="post" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">用户名</label>
                <div class="col-sm-3">
                    <form:input path="loginName" htmlEscape="false" readonly="true" maxlength="50" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">邮箱</label>
                <div class="col-sm-3">
                    <form:input path="email" htmlEscape="false" maxlength="50" class="required email form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">电话</label>
                <div class="col-sm-3">
                    <form:input path="phone" htmlEscape="false" maxlength="50" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">备注</label>
                <div class="col-sm-4">
                    <form:textarea path="remarks" htmlEscape="false" class="form-control" rows="3"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">用户角色</label>
                <div class="col-sm-5">
                    <label class="control-label">${user.roleNames}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">上次登陆</label>
                <div class="col-sm-5">
                    <label class="control-label">IP: ${user.oldLoginIp}&nbsp;&nbsp;&nbsp;&nbsp;时间：<fmt:formatDate value="${user.oldLoginDate}" type="both" dateStyle="full"/></label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-sm loading-demo mr5" >
                <i class="ti-share mr5"></i>Submit</button>
        </form:form>
    </div>
</body>
</html>
