<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>业务表列表</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        function page(n,s){
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/user/list");
            $("#searchForm").submit();
            return false;
        }
    </script>
</head>
<body>
<header class="panel-heading no-b">
    <h5><b>业务表列表</b></h5>
</header>
<div class="panel-body">
    <div class="table-responsive">
        <form:form id="searchForm" modelAttribute="genTable" action="${ctx}/gen/genTable" method="post" class="form-inline" cssStyle="padding-bottom: 10px">
            <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
            <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
            <label>表名：</label><form:input path="nameLike" htmlEscape="false" maxlength="50" class="form-control"/>
            <label>说明：</label><form:input path="comments" htmlEscape="false" maxlength="50" class="form-control"/>
            <label>父表表名：</label><form:input path="parentTable" htmlEscape="false" maxlength="50" class="form-control"/>
            <button type="submit" class="btn btn-primary">搜索</button>
            <label><a href="${ctx}/gen/genTable/form?id=${genTable.id}&name=${genTable.name}" class="btn btn-primary">新增业务表</a></label>
        </form:form>
        <sys:message content="${message}"/>
        <table class="table table-striped table-bordered responsive no-m" data-sortable>
            <thead><tr><th>表名</th><th>说明</th><th>类名</th><th>父表</th><shiro:hasPermission name="gen:genTable:edit"><th>操作</th></shiro:hasPermission></tr></thead>
            <tbody>
            <c:forEach items="${page.list}" var="genTable">
                <tr>
                    <td><a href="${ctx}/gen/genTable/form?id=${genTable.id}">${genTable.name}</a></td>
                    <td>${genTable.comments}</td>
                    <td>${genTable.className}</td>
                    <td title="点击查询子表"><a href="javascript:" onclick="$('#parentTable').val('${genTable.parentTable}');$('#searchForm').submit();">${genTable.parentTable}</a></td>
                    <shiro:hasPermission name="gen:genTable:edit"><td>
                        <a href="${ctx}/gen/genTable/form?id=${genTable.id}">修改</a>
                        <a href="${ctx}/gen/genTable/delete?id=${genTable.id}" onclick="return confirmx('确认要删除该业务表吗？', this.href)">删除</a>
                    </td></shiro:hasPermission>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="text-center">${page}</div>
    </div>
</div>
</body>
</html>
