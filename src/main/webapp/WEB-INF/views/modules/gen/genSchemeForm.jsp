<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>生成方案管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#name").focus();
            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
</head>
<body>
    <header class="panel-heading">生成方案管理</header>
    <div class="panel-body">
        <form:form id="inputForm" modelAttribute="genScheme" action="${ctx}/gen/genScheme/save" method="post" class="form-horizontal">
            <%--<sys:message content="${message}"/>--%>
            <form:hidden path="id"/><form:hidden path="flag"/>
            <div class="form-group">
                <label class="col-sm-2 control-label">方案名称：</label>
                <div class="col-sm-3">
                    <form:input path="name" htmlEscape="false" maxlength="200" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">模板分类：</label>
                <div class="col-sm-3">
                    <form:select path="category" style="width:100%;" class="required input-medium">
                        <form:options items="${config.categoryList}" itemLabel="label" itemValue="value" htmlEscape="false"/>
                    </form:select>
				</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成包路径：</label>
                <div class="col-sm-3">
                    <form:input path="packageName" htmlEscape="false" maxlength="500" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成模块名：</label>
                <div class="col-sm-3">
                    <form:input path="moduleName" htmlEscape="false" maxlength="500" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成子模块名：</label>
                <div class="col-sm-3">
                    <form:input path="subModuleName" htmlEscape="false" maxlength="500" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成功能描述：</label>
                <div class="col-sm-4">
                    <form:input path="functionName" htmlEscape="false" maxlength="500" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成功能名：</label>
                <div class="col-sm-4">
                    <form:input path="functionNameSimple" htmlEscape="false" maxlength="500" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">生成功能作者：</label>
                <div class="col-sm-4">
                    <form:input path="functionAuthor" htmlEscape="false" maxlength="500" class="required form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">业务表名：</label>
                <div class="col-sm-4">
                    <form:select path="genTable.id" style="width:100%;" class="required input-medium">
                        <form:options items="${tableList}" itemLabel="nameAndComments" itemValue="id" htmlEscape="false"/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">备注</label>
                <div class="col-sm-4">
                    <form:textarea path="remarks" htmlEscape="false" class="form-control" rows="3"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">生成选项：</label>
                <div class="col-sm-4">
                    <form:checkbox path="replaceFile" label="是否替换现有文件"/>
                </div>
            </div>
            <shiro:hasPermission name="gen:genScheme:edit">
                <input id="btnSubmit" class="btn btn-primary" type="submit" value="保存方案" onclick="$('#flag').val('0');"/>&nbsp;
                <input id="btnSubmit" class="btn btn-danger" type="submit" value="保存并生成代码" onclick="$('#flag').val('1');"/>&nbsp;
            </shiro:hasPermission>
            <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
        </form:form>
    </div>
</body>
</html>
