<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>生成方案</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        function page(n,s){
            if(n) $("#pageNo").val(n);
            if(s) $("#pageSize").val(s);
            $("#searchForm").attr("action","${ctx}/sys/user/list");
            $("#searchForm").submit();
            return false;
        }
    </script>
</head>
<body>
<header class="panel-heading no-b">
    <h5><b>生成方案列表</b></h5>
</header>
<div class="panel-body">
    <div class="table-responsive">
        <form:form id="searchForm" action="${ctx}/gen/genScheme" method="post" class="form-inline" cssStyle="padding-bottom: 10px">
            <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
            <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
            <label>名称：</label><input id="title" name="name" type="search" class="form-control" maxlength="50" value="${genScheme.name}"/>
            <button type="submit" class="btn btn-primary">搜索</button>
            <label><a href="${ctx}/gen/genScheme/form" class="btn btn-primary">生成方案新增</a></label>
        </form:form>
        <sys:message content="${message}"/>
        <table class="table table-striped table-bordered responsive no-m" data-sortable>
            <thead><tr><th>方案名称</th><th>生成模块</th><th>模块名</th><th>功能名</th><th>功能作者</th><shiro:hasPermission name="gen:genScheme:edit"><th>操作</th></shiro:hasPermission></tr></thead>
            <tbody>
            <c:forEach items="${page.list}" var="genScheme">
                <tr>
                    <td><a href="${ctx}/gen/genScheme/form?id=${genScheme.id}">${genScheme.name}</a></td>
                    <td>${genScheme.packageName}</td>
                    <td>${genScheme.moduleName}${not empty genScheme.subModuleName?'.':''}${genScheme.subModuleName}</td>
                    <td>${genScheme.functionName}</td>
                    <td>${genScheme.functionAuthor}</td>
                    <shiro:hasPermission name="gen:genScheme:edit"><td>
                        <a href="${ctx}/gen/genScheme/form?id=${genScheme.id}">修改</a>
                        <a href="${ctx}/gen/genScheme/delete?id=${genScheme.id}" onclick="return confirmx('确认要删除该生成方案吗？', this.href)">删除</a>
                    </td></shiro:hasPermission>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="text-center">${page}</div>
    </div>
</div>
</body>
</html>
