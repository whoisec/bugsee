package cn.bugsee.modules.gen.dao;

import cn.bugsee.common.persistence.CrudDao;
import cn.bugsee.common.persistence.annotation.MyBatisDao;
import cn.bugsee.modules.gen.entity.GenTemplate;

@MyBatisDao
public interface GenTemplateDao extends CrudDao<GenTemplate> {
	
}
