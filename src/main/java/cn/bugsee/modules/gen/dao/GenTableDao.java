package cn.bugsee.modules.gen.dao;

import cn.bugsee.common.persistence.CrudDao;
import cn.bugsee.common.persistence.annotation.MyBatisDao;
import cn.bugsee.modules.gen.entity.GenTable;

@MyBatisDao
public interface GenTableDao extends CrudDao<GenTable> {
	
}
