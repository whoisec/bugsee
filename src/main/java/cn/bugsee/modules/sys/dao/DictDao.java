package cn.bugsee.modules.sys.dao;

import cn.bugsee.common.persistence.CrudDao;
import cn.bugsee.common.persistence.annotation.MyBatisDao;
import cn.bugsee.modules.sys.entity.Dict;

import java.util.List;

@MyBatisDao
public interface DictDao extends CrudDao<Dict> {

	public List<String> findTypeList(Dict dict);
	
}
