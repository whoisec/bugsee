package cn.bugsee.modules.sys.dao;

import cn.bugsee.common.persistence.CrudDao;
import cn.bugsee.common.persistence.annotation.MyBatisDao;
import cn.bugsee.modules.sys.entity.Log;

@MyBatisDao
public interface LogDao extends CrudDao<Log> {

}
