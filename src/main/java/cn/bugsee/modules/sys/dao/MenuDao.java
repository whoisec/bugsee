package cn.bugsee.modules.sys.dao;

import cn.bugsee.common.persistence.CrudDao;
import cn.bugsee.common.persistence.annotation.MyBatisDao;
import cn.bugsee.modules.sys.entity.Menu;

import java.util.List;

@MyBatisDao
public interface MenuDao extends CrudDao<Menu> {

	public List<Menu> findByParentIdsLike(Menu menu);

	public List<Menu> findByUserId(Menu menu);
	
	public int updateParentIds(Menu menu);
	
	public int updateSort(Menu menu);
	
}
